#!/usr/bin/env python

"""
This python is taking and image from the webcam. Applies some filters
and then publishing this image to a topic
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__version__=  '0.1'
__license__ = 'BSD'

import sys, time
import numpy as np
import cv2
import rospy
from sensor_msgs.msg import CompressedImage
#from cv_bridge import CvBridge, CvBridgeError

def mymain():
	rospy.init_node('image_feature', anonymous=True)
	image_pub = rospy.Publisher("/image_raw/compressed", CompressedImage, queue_size=1)
	# Looping at
	rate = rospy.Rate(30) # 30hz
	'''Initializes and cleanup ros node'''
	cap = cv2.VideoCapture(0)
	cap.set(3,320)
	cap.set(4,240)
	# cap.set(16, 0.0)
	while not rospy.is_shutdown():
		# Get Frame
		ret, frame = cap.read()
		#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		r, crsd = cv2.imencode('.jpg', frame)
		myuint8 = np.asarray(crsd, dtype="uint8").tostring()
		msg = CompressedImage()
		msg.format = "jpeg"
		msg.data = myuint8
		#Publish
		image_pub.publish(msg)
		# Preserve looping in specified rate
		rate.sleep()

if __name__ == '__main__':
	try:
		mymain()
	except rospy.ROSInterruptException:
		print "Shutting down ROS Image feature detector module"
		cap.release()
		cv2.destroyAllWindows()
