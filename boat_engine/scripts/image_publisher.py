#!/usr/bin/env python

"""
This python is taking and image from the webcam. Applies some filters
and then publishing this image to a topic
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__version__=  '0.1'
__license__ = 'BSD'

import sys, time
import numpy as np
import cv2
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

def mymain():
	'''Initializes and cleanup ros node'''
	rospy.init_node('image_feature', anonymous=True)
	image_pub = rospy.Publisher("/image_raw/raw", Image, queue_size=1)
	cap = cv2.VideoCapture(0)
	cap.set(3,320)
	cap.set(4,240)
	# cap.set(16, 0.0)
	bridge = CvBridge()
	while not rospy.is_shutdown():
		# Get Frame
		ret, frame = cap.read()
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		#cv2.imshow("debug", gray)
		#frame_reduced = cv2.resize(gray, (320, 240), interpolation = cv2.INTER_CUBIC)
		image_message = bridge.cv2_to_imgmsg(gray, "mono8")
		#Publish
		image_pub.publish(image_message)

if __name__ == '__main__':
	try:
		mymain()
	except rospy.ROSInterruptException:
		print "Shutting down ROS Image feature detector module"
		cap.release()
		cv2.destroyAllWindows()
