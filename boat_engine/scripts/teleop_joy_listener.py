#!/usr/bin/env python

"""
Subscriber for any Gamepad, Joystick
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__license__ = 'GPLv3'
import RPi.GPIO as GPIO
import rospy
from sensor_msgs.msg import Joy

GPIO.setmode(GPIO.BCM)

horn = 13
lights = 6

lights_state = 0
lights_debouncer_time = 0

GPIO.setup(horn, GPIO.OUT)
GPIO.setup(lights, GPIO.OUT)

HornPWM=GPIO.PWM(horn, 100)
HornPWM.ChangeFrequency(200)
HornPWM.start(0) 

#PS2 Gamepad Configuration
ps2 = {}
ps2["threshold"] = 0.20

ps2["buttons"] = {}
ps2["buttons"]["cross"] 	= 2
ps2["buttons"]["square"] 	= 3
ps2["buttons"]["circle"] 	= 1
ps2["buttons"]["triangle"] 	= 0
ps2["buttons"]["start"]		= 9
ps2["buttons"]["select"]	= 8
ps2["buttons"]["l1"]		= 6
ps2["buttons"]["l2"]		= 4
ps2["buttons"]["r1"]		= 7
ps2["buttons"]["r2"]		= 5
ps2["buttons"]["l3"]		= 10
ps2["buttons"]["r3"]		= 11

ps2["axes"] = {}
ps2["axes"]["lx_axis"]		= 0
ps2["axes"]["ly_axis"]		= 1
ps2["axes"]["ry_axis"]		= 2
ps2["axes"]["rx_axis"]		= 3

def callback(data):
	global lights_state
	global lights_debouncer_time

	horn_value = data.buttons[ps2["buttons"]["cross"]]
	lights_value = data.buttons[ps2["buttons"]["square"]]

	if (horn_value == 1):
		HornPWM.start(70)
	elif (horn_value == 0):
		HornPWM.stop()

	if (lights_value == 1):
		current_time = rospy.get_time()
		if (current_time > (lights_debouncer_time + 0.5)):
			if (lights_state == 1):
				GPIO.output(lights, GPIO.LOW)
				lights_state = 0
			else:
				GPIO.output(lights, GPIO.HIGH)
				lights_state = 1
			lights_debouncer_time = rospy.get_time()
	
	

def listener():
	# Initiate Node
	rospy.init_node('teleop_boat_engine', anonymous=True)
	# Subscribe to joystick topic
	rospy.Subscriber('/joy_teleop', Joy, callback)

	try:
		rospy.spin()
	except rospy.ROSInterruptException:
		GPIO.cleanup()
		pass

if __name__ == '__main__':
	listener()