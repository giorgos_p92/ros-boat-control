#!/usr/bin/env python

import RPi.GPIO as GPIO
import rospy
import sys,time

GPIO.setmode(GPIO.BCM)

Motor1A = 17 #This is the pwm
Motor1B = 27
Motor1E = 22

Motor2A = 25 #This is the pwm
Motor2B = 24
Motor2E = 23

GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor1E,GPIO.OUT)

GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)
GPIO.setup(Motor2E,GPIO.OUT)

#Initiate
LeftMotorPWM=GPIO.PWM(Motor1A, 100)
RightMotorPWM=GPIO.PWM(Motor2A, 100)
LeftMotorPWM.start(0)
RightMotorPWM.start(0)

try:
	while True:
		#Left engine goes forward
		GPIO.output(Motor1B,GPIO.HIGH)
		GPIO.output(Motor1E,GPIO.LOW)
		#Right engine goes Backward
		GPIO.output(Motor2B,GPIO.LOW)
		GPIO.output(Motor2E,GPIO.HIGH)
		#spin
		LeftMotorPWM.ChangeDutyCycle(50)
		RightMotorPWM.ChangeDutyCycle(50)
		time.sleep(1)
		GPIO.cleanup()
		sys.exit()
except KeyboardInterrupt:
	GPIO.cleanup()
	sys.exit()