#!/usr/bin/env python

"""
Motor Subscriber
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__license__ = 'GPLv3'

import RPi.GPIO as GPIO
import rospy
from boat_engine.msg import axis
from geometry_msgs.msg import Twist

GPIO.setmode(GPIO.BCM)

Motor1A = 17 #This is the pwm
Motor1B = 27
Motor1E = 22

Motor2A = 25 #This is the pwm
Motor2B = 24
Motor2E = 23

horn = 13

lights = 6

GPIO.setup(Motor1A,GPIO.OUT)
GPIO.setup(Motor1B,GPIO.OUT)
GPIO.setup(Motor1E,GPIO.OUT)

GPIO.setup(Motor2A,GPIO.OUT)
GPIO.setup(Motor2B,GPIO.OUT)
GPIO.setup(Motor2E,GPIO.OUT)

GPIO.setup(horn, GPIO.OUT)
GPIO.setup(lights, GPIO.OUT)

#Initiate
LeftMotorPWM=GPIO.PWM(Motor1A, 100)
RightMotorPWM=GPIO.PWM(Motor2A, 100)
LeftMotorPWM.start(0)
RightMotorPWM.start(0)

HornPWM=GPIO.PWM(horn, 100)
HornPWM.ChangeFrequency(200)
HornPWM.start(0) 

lights_state = 0
lights_debouncer_time = 0

#PS2 Gamepad Configuration
ps2 = {}
ps2["threshold"] = 0.20

ps2["buttons"] = {}
ps2["buttons"]["cross"] 	= 2
ps2["buttons"]["square"] 	= 3
ps2["buttons"]["circle"] 	= 1
ps2["buttons"]["triangle"] 	= 0
ps2["buttons"]["start"]		= 9
ps2["buttons"]["select"]	= 8
ps2["buttons"]["l1"]		= 6
ps2["buttons"]["l2"]		= 4
ps2["buttons"]["r1"]		= 7
ps2["buttons"]["r2"]		= 5
ps2["buttons"]["l3"]		= 10
ps2["buttons"]["r3"]		= 11

ps2["axes"] = {}
ps2["axes"]["lx_axis"]		= 0
ps2["axes"]["ly_axis"]		= 1
ps2["axes"]["ry_axis"]		= 2
ps2["axes"]["rx_axis"]		= 3

ps2["hat"] = 0

def callback(data):
	global lights_state
	global lights_debouncer_time
	
	vLeftRight = data.angular.z
	vForwardBackward = data.linear.x

	leftMotorCommand = vForwardBackward - vLeftRight*0.8
	rightMotorCommand = vForwardBackward + vLeftRight*0.8

	if (leftMotorCommand > 0):
		GPIO.output(Motor1B,GPIO.HIGH)
		GPIO.output(Motor1E,GPIO.LOW)
	elif (leftMotorCommand < 0):
		GPIO.output(Motor1B,GPIO.LOW)
		GPIO.output(Motor1E,GPIO.HIGH)
		leftMotorCommand = leftMotorCommand * (-1)

	if (leftMotorCommand > 1):
			leftMotorCommand = 1

	if (rightMotorCommand > 0):
		GPIO.output(Motor2B,GPIO.HIGH)
		GPIO.output(Motor2E,GPIO.LOW)
	elif (rightMotorCommand < 0):
		GPIO.output(Motor2B,GPIO.LOW)
		GPIO.output(Motor2E,GPIO.HIGH)
		rightMotorCommand = rightMotorCommand * (-1)

	if (rightMotorCommand > 1):
			rightMotorCommand = 1

	LeftMotorPWM.ChangeDutyCycle(leftMotorCommand*100)
	RightMotorPWM.ChangeDutyCycle(rightMotorCommand*100)
	"""
	if (data.horn == 1):
		HornPWM.start(70)
	elif (data.horn == 0):
		HornPWM.stop()

	if (data.lights == 1):
		current_time = rospy.get_time()
		if (current_time > (lights_debouncer_time + 0.5)):
			if (lights_state == 1):
				GPIO.output(lights, GPIO.LOW)
				lights_state = 0
			else:
				GPIO.output(lights, GPIO.HIGH)
				lights_state = 1
			lights_debouncer_time = rospy.get_time()
	"""

def listener():
	rospy.init_node('boat_engine_motor_control', anonymous=True)

	rospy.Subscriber('/cmd_vel', Twist, callback)
	try:
		rospy.spin()
	except rospy.ROSInterruptException:
		GPIO.cleanup()
		pass

if __name__ == '__main__':
	listener()