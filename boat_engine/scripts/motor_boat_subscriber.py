#!/usr/bin/env python

"""
Motor Subscriber
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__license__ = 'GPLv3'

import RPi.GPIO as GPIO
import rospy
from boat_engine.msg import axis
from geometry_msgs.msg import Twist

GPIO.setmode(GPIO.BCM)


MotorFRA = 25 #This is the pwm
MotorFRB = 24
MotorFRE = 23

SteeringServoPin = 26
# MotorLRA = 17 #This is the pwm
# MotorLRB = 27
# MotorLRE = 22

GPIO.setup(MotorFRA,GPIO.OUT)
GPIO.setup(MotorFRB,GPIO.OUT)
GPIO.setup(MotorFRE,GPIO.OUT)

GPIO.setup(SteeringServoPin,GPIO.OUT)
#Inititate
PropulsionEngine=GPIO.PWM(MotorFRA, 100)
PropulsionEngine.start(0)

SteeringServo=GPIO.PWM(SteeringServoPin, 50)
SteeringServo.start(7.5) 

#PS2 Gamepad Configuration
ps2 = {}
ps2["threshold"] = 0.20

ps2["buttons"] = {}
ps2["buttons"]["cross"] 	= 2
ps2["buttons"]["square"] 	= 3
ps2["buttons"]["circle"] 	= 1
ps2["buttons"]["triangle"] 	= 0
ps2["buttons"]["start"]		= 9
ps2["buttons"]["select"]	= 8
ps2["buttons"]["l1"]		= 6
ps2["buttons"]["l2"]		= 4
ps2["buttons"]["r1"]		= 7
ps2["buttons"]["r2"]		= 5
ps2["buttons"]["l3"]		= 10
ps2["buttons"]["r3"]		= 11

ps2["axes"] = {}
ps2["axes"]["lx_axis"]		= 0
ps2["axes"]["ly_axis"]		= 1
ps2["axes"]["ry_axis"]		= 2
ps2["axes"]["rx_axis"]		= 3

ps2["hat"] = 0

def callback(data):
	
	Helm = data.angular.z*(-1)
	Throttle = data.linear.x*(-1)


	print "Linear X %s  Angular Z  %s" % (data.linear.x, data.angular.z)

	if (Throttle > 0):
		GPIO.output(MotorFRB,GPIO.LOW)
		GPIO.output(MotorFRE,GPIO.HIGH)
	elif (Throttle < 0):
		GPIO.output(MotorFRB,GPIO.HIGH)
		GPIO.output(MotorFRE,GPIO.LOW)
		Throttle = Throttle * (-1)

	if (Throttle > 1):
			Throttle = 1

	
	if (Helm > 1):
		Helm = 1

	if (Helm > ps2["threshold"]):
		steeringcommand = 7.5-(Helm*1.9)
		SteeringServo.ChangeDutyCycle(steeringcommand)
	elif (Helm < -ps2["threshold"]):
		steeringcommand = 7.5-(Helm*1.8)
		SteeringServo.ChangeDutyCycle(steeringcommand)
	elif Helm > -ps2["threshold"] and Helm < ps2["threshold"]:
		SteeringServo.ChangeDutyCycle(7.3)

	PropulsionEngine.ChangeDutyCycle(Throttle*100)


if __name__ == '__main__':
	try:
		rospy.init_node('boat_engine_motor_control', anonymous=True)
		rospy.Subscriber('/cmd_vel', Twist, callback)
		rospy.spin()
	except rospy.ROSInterruptException:
		print "Exiting..."
		PropulsionEngine.stop()
		SteeringServo.stop()
		GPIO.cleanup()
		pass
