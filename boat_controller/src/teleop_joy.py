#!/usr/bin/env python

"""
Publisher for any Gamepad, Joystick using pygame python library
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__license__ = 'GPLv3'

import rospy
import pygame
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist

pygame.init()
# Just the first joystick
j = pygame.joystick.Joystick(0)
# Initiate
j.init()
# Debug to user
print 'Initialized Joystick : %s' % j.get_name()

def stroker():
	# Looping at
	rate = rospy.Rate(10) # 10hz
	
	while not rospy.is_shutdown():
		pygame.event.pump()

		# Empty values arrays
		axes_values = []
		buttons_values = []
		
		# Create msg object
		msg = Joy()
		
		# Assign current time in header
		#msg.header = rospy.get_time()
		
		# Iterate axes and populate value arrays
		for axis in range(j.get_numaxes()):
			axes_values.insert(axis, j.get_axis(axis))
		for button in range(j.get_numbuttons()):
			buttons_values.insert(button, j.get_button(button))
		
		# Populate message
		msg.axes = axes_values
		msg.buttons = buttons_values
		
		# Publish message
		pub_joy.publish(msg)
		
		# Preserve looping in specified rate
		rate.sleep()

if __name__ == '__main__':
	# Initiate Node
	rospy.init_node('teleop_rojo_control', anonymous=True)
	# Publisher
	pub_joy = rospy.Publisher('/joy_teleop', Joy, queue_size=1)
	try:
		stroker()
	except rospy.ROSInterruptException:
		pass
