#!/usr/bin/env python

"""
This python is for viewing comprossed images from a robot
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__license__ = 'GPLv3'

import sys, time
import numpy as np
import cv2
import rospy
from sensor_msgs.msg import CompressedImage
# from cv_bridge import CvBridge, CvBridgeError

class image_converter:
	def __init__(self):
		# self.bridge = CvBridge()
		self.image_sub = rospy.Subscriber("/image_raw/compressed",CompressedImage,self.callback)
	def callback(self,data):
		nparr = np.fromstring(data.data, np.uint8)
		new_image = cv2.imdecode(nparr, 1)
		new_image2 = cv2.resize(new_image, (1280, 720), interpolation = cv2.INTER_NEAREST)
		cv2.imshow("Image window", new_image2)
		cv2.waitKey(30)
def main(args):
	'''Initializes and cleanup ros node'''
	ic = image_converter()
	rospy.init_node('image_feature', anonymous=True)
	try:
		rospy.spin()	
	except KeyboardInterrupt:
		print "Shutting down ROS Image feature detector module"
		cv2.destroyAllWindows()

if __name__ == '__main__':
	main(sys.argv)