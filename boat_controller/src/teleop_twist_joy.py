#!/usr/bin/env python

"""
Subscriber for gamepad, joystick and publisher for converted values to geometry_msgs Twist
"""

__author__ =  'Giorgos Patsiaouras <giorgospatsiaouras@gmail.com>'
__license__ = 'GPLv3'

import rospy
import pygame
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist

# Receives data from the gamepad
def teleopSubscriber(data):
	cmd_vel_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
	msg = Twist()
	msg.linear.x = data.axes[2]*(-1)
	msg.angular.z = data.axes[3]*(-1)
	#print "Linear X %s  Angular Z  %s" % (msg.linear.x, msg.angular.z)
	cmd_vel_pub.publish(msg)
	#rospy.loginfo("lineaR: %s, Angular %s", msg.linear.x, msg.angular.z)

if __name__ == '__main__':
	try:
		rospy.init_node('rojo_teleop_control', anonymous=True)
		rospy.Subscriber('/joy_teleop', Joy, teleopSubscriber)
		rospy.spin()
	except rospy.ROSInterruptException:
		pass
