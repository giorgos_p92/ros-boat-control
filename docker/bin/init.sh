#!/bin/bash

if [ $ROLE = "master" ]; then
	echo "is master"
	roscore
elif [ $ROLE = "slave" ]; then
	echo "is slave"
	(cd /root/catkin_ws && catkin_make)
	echo 'source /root/catkin_ws/devel/setup.bash' >> /root/.bashrc 
	source /root/catkin_ws/devel/setup.bash
	# rosrun boat_controller teleop_joy.py &
	# rosrun boat_controller teleop_twist_joy.py &
	tail -f /dev/null
fi